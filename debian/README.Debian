The pychecker Python package is installed as a public package by the
dh_python2 infrastructure.  This means that you can import pychecker
modules directly from any "current" version of Python.  (The "current" version
of Python is defined by the Debian Python policy and the dh_python2
infrastructure.  As of this writing, Python 2.6 and 2.7 are current.)

This package does still support use of $PYTHONVER on the command-line.  You can
use this functionality to specify which version of Python the pychecker code
should be executed with.  For instance, you can use something like this:

   PYTHONVER=2.6 pychecker file.py

Unfortunately, now that the new Python infrastructure is in place, this
functionality is less helpful than it used to be.  A user can no longer invoke
any arbitrary python interpreter installed in /usr/bin.  Only one of the
"current" Python interpreters will work.

The pychecker2 code is also included in the Debian package.  Pychecker itself
(the utility) does not use this code.  However, the Debian package for SPE uses
it.  Upstream is OK with me including this code in the Debian package, as long
as everyone is aware that pychecker2 is not under active development.

Kenneth J. Pronovici <pronovic@debian.org>
24 Apr 2013
